const canvas = document.getElementById('pong')
const ctx = canvas.getContext('2d')

class Pad {

	/**
     * Constructeur d'un Pad
     * @param {boolean} isGauche - Le pad est à gauche ou à droite. 
     */
	constructor(isGauche, k) {
		this.isGauche = isGauche
		this.w = 2
		this.h = 100
		this.y = (canvas.clientHeight / 2) - (this.h / 2)
		this.score = 0
		if(isGauche) {
			this.x = 10
		} else {
			this.x = canvas.clientWidth - 10 - this.w
		}
		this.k = k
	}

	dessiner() {
		ctx.fillStyle = 'white'
		ctx.fillRect(this.x, this.y, this.w, this.h)
		ctx.fill()
		// dessin du score
		ctx.fillStyle = 'white'
		ctx.textAlign = 'center'
		if(this.isGauche) {
			ctx.fillText(this.score, (canvas.clientWidth / 2) - 20, 20)
		} else {
			ctx.fillText(this.score, (canvas.clientWidth / 2) + 20, 20)
		}
		ctx.fill()
	}

	seDeplacer(direction) {
		if(direction === 'haut') {
			console.log('h')
			this.y -= 10
		} else if(direction === 'bas'){
			console.log('b')
			this.y += 10
		}
	}
}

class Balle {
	constructor() {
		this.x = canvas.clientWidth / 2
		this.y = canvas.clientHeight / 2
		this.r = 10
		this.v = 5
		this.dx = Math.round( Math.random() * 1) === 0 ? -1 : 1
		this.dy = Math.round( Math.random() * 1) === 0 ? -1 : 1
	}

	dessiner() {
		ctx.fillStyle = 'white'
		ctx.beginPath()
		ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI)
		ctx.closePath()
		ctx.fill()
	}

	seDeplacer() {
		const positionSuivante = { x: this.x + this.dx, y: this.y + this.dy }
		// collision mur haut
		if(positionSuivante.y - this.r < 0) {
			this.y = 0 + this.r
			this.dy = -this.dy
		} else if(positionSuivante.y + this.r > canvas.clientHeight) {
			this.y = canvas.clientHeight - this.r
			this.dy = -this.dy
		}

		// collision p1
		if(this.dx < 0) {
			if(this.x - this.r < pad1.x + pad1.w) {
				if(this.y > pad1.y && this.y < pad1.y + pad1.h) {
					this.v += 0.1
					this.x = pad1.x + pad1.w
					this.dx = -this.dx
				}
			}
			// collision p2
		} else {
			if(this.x + this.r > pad2.x) {
				if(this.y > pad2.y && this.y < pad2.y + pad2.h) {
					this.v += 0.1 // acceleration
					this.x = pad2.x // replacement au bord du pad
					this.dx = -this.dx // changement de direction
				}
			}
		}

		if(positionSuivante.x < 0) {
			pad2.score++
			this.reset()
		} else if( positionSuivante.x > canvas.clientWidth) {
			pad1.score++
			this.reset()
		}

		this.x = Math.round(this.x + (this.dx * this.v))
		this.y = Math.round(this.y + (this.dy * this.v))
	}

	reset() {
		this.x = canvas.clientWidth / 2
		this.y = canvas.clientHeight /2
		this.v = 20
		this.dx = Math.round( Math.random() * 1) === 0 ? -1 : 1
		this.dy = Math.round( Math.random() * 1) === 0 ? -1 : 1
	}

}

const TR = {
	ENDPOINTS: {
		MESSAGES: 'msgs',
		PLAYERS: 'players',
		PING: 'pings'
	},
	METHODS: {
		GET: 'GET',
		POST: 'POST'
	}
}

function Xhr(method, url) {
	return new Promise( (resolve,reject) => {
		const request = new XMLHttpRequest()
		request.open(method, url, false)
		request.send()
		if(request.status === 200) {
			resolve(request.responseText)
		} else {
			reject(`XHR [${request.status}]: ${request.statusText}`)
		}
	})
}
/**
 * Faire une requête pour le temps réel
 * @param {string} endpoint
 * @param {string} method - TR.GET ou TR.POST 
 * @param {...any} args 
 */
function Request(endpoint, method, args) {
    return new Promise( (resolve,reject) => {
		let url = "http://syllab.com/PTRE839/" + endpoint
		let arg = ''
		if(args) {
			arg = '?' + args.map( a => {return `${a.k}=${a.v}`}).join('&')
		}
		url += arg
		Xhr(method, url)
			.then( r => resolve(r))
			.catch( err => reject(err))
	})
}

function players(key) {
    return new Promise( (resolve,reject) => {
		Request(TR.ENDPOINTS.PLAYERS, TR.METHODS.GET, [{k: 'k', v: key}])
			.then( res => resolve(res))
			.catch( err => reject(err))
	})
}

function player(key, id) {
    return new Promise( (resolve,reject) => {
		Request(TR.ENDPOINTS.PLAYERS + '/' + id, TR.METHODS.GET, [{k: 'k', v: key}])
			.then( res => resolve(res))
			.catch( err => reject(err))
	})
}

function message(key, timeout) {
    return new Promise( (resolve,reject) => {
		Request(TR.ENDPOINTS.MESSAGES, TR.METHODS.GET, [{k: 'k', v: key},{k: 'timeout', v: timeout}])
			.then( res => resolve(res))
			.catch( err => reject(err))
	})
}

function send(key, to, data) {
    return new Promise( (resolve,reject) => {
		Request(TR.ENDPOINTS.MESSAGES, TR.METHODS.POST, [{k: 'k', v: key}, {k: 'to', v: to}, {k: 'data', v: JSON.stringify(data)}])
			.then( res => resolve(res))
			.catch( err => reject(err))
	})
}

function ping(key, t0) {
	return new Promise( (resolve,reject) => {
		Request(TR.ENDPOINTS.PING, TR.METHODS.GET, [{k: 'k', v: key},{k: 't0', v: t0}])
			.then( res => resolve(res))
			.catch( err => reject(err))
	})
}

function dessinerTerrain() {
	ctx.clearRect(0,0, canvas.clientWidth, canvas.clientHeight)
	ctx.fillStyle = 'black'
	ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight)
	ctx.fill()
	ctx.fillStyle = 'white'
	ctx.fillRect(canvas.clientWidth / 2, 0, 1, canvas.clientHeight)
	ctx.fill()
}

const pad1 = new Pad(true, '19476')
const pad2 = new Pad(false, '25403')
const balle = new Balle()
let paused = false

function gererPause() {
	document.addEventListener( 'keyup', (e) => {
		if(e.key === 'p' || e.key === 'P') {
			paused = !paused
		}
	})
	if(paused) {
		ctx.font = '20px'
		ctx.textAlign = 'center'
		ctx.fillStyle = 'white'
		ctx.fillText('Pause. \'P\' pour reprendre.', canvas.clientWidth / 2, canvas.clientHeight / 2)
		ctx.font = '20px'
		ctx.textAlign = 'center'
		ctx.fillStyle = 'white'
		ctx.fillText('Commandes: Souris./!\\ Smartphone non-compatible.', canvas.clientWidth / 2, canvas.clientHeight / 2 + 25)
		ctx.fill()
	}
}

function gererDeplacement() {
		balle.seDeplacer()
		
		pad2.y = balle.y - pad2.h / 2
}

function calcPing() {
	return new Promise( (resolve,reject) => {
		const t0 = new Date().getTime()
		ping(pad1.k, t0)
			.then( r => {
				const result = JSON.parse(r)
				const t3 = new Date().getTime()
				resolve(Math.round(((result.t1 - t0) + (t3 - result.t2)) / 2))
			})
			.catch( err => reject(err))
	})
}

setInterval( function() {
	// TR
	send(pad1.k, pad2.k, {date: new Date().getTime(), y: pad1.y})
		.then( r => {
			console.log(r)
			console.log('Envoyé')
		})
		.catch( err => {
			console.error(err)
		})
	
	// DESSIN
	dessinerTerrain()
	pad1.dessiner()
	pad2.dessiner()
	balle.dessiner()
	
	if(!paused) {
		gererDeplacement()
	}
}, 20)

document.addEventListener('mousemove', (e) => {
	if(!paused) {
		const y = e.clientY
		if(y > canvas.clientHeight - pad1.h) {
			pad1.y = canvas.clientHeight - pad1.h
		} else {
			pad1.y = y
		}
	}
})

function getPosition() {
	const r = message(pad2.k, 5000)
		.then( v => {
			pad2.y = JSON.parse(r).y
			getPosition()
		})
		.catch( err => {
			console.error(err)
		})	
}

setInterval( function() {
	calcPing()
		.then( res => console.log(res) )
		.catch( err => console.error(err))
}, 1000)
